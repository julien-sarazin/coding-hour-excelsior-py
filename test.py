#!/usr/bin/python3.5

import unittest
from sources.excelsior import excelsior as sut


class Test(unittest.TestCase):
    def setUp(self):
        pass

    def test_when_an_invalid_order_is_made_it_should_return_an_empty_weapon(self):
        with open('invalid_order.json', 'r') as data:
            weapon = sut(data)

        self.assertEqual(weapon['damage'], None)
        self.assertEqual(weapon['speed'], None)
        self.assertEqual(isinstance(weapon['spells'], list), True)
        self.assertEqual(len(weapon['spells']), 0)

    def test_when_an_simple_order_is_made_it_should_craft_a_weapon_with_proper_attributes(self):
        with open('simple_order.json', 'r') as data:
            weapon = sut(data)

        self.assertEqual(weapon['damage'], 10)
        self.assertEqual(weapon['speed'], 3.55)
        self.assertEqual(isinstance(weapon['spells'], list), True)
        self.assertEqual(len(weapon['spells']), 2)

    def test_when_a_complex_order_is_made_with_more_than_one_triggering_spell_it_should_craft_a_weapon_with_proper_attributes(self):
        with open('complex_order.json') as data:
            weapon = sut(data)

        self.assertEqual(isinstance(weapon['spells'], list), True)
        self.assertEqual(len(weapon['spells']), 1)

        spell_1 = weapon['spells'][0]
        self.assertEqual(spell_1['spell'],'cold_weaknesses')
        self.assertEqual(spell_1['trigger'], {'spell': "frozen_ground ice_bolt added_cold_damage"});

    def test_when_a_very_complex_order_is_made_with_multiple_combined_nested_triggering_spells_it_should_craft_a_weapon_with_proper_attributes(self):
        with open('very_complex_order.json', 'r') as data:
            weapon = sut(data)

        self.assertEqual(isinstance(weapon['spells'], list), True)
        self.assertEqual(len(weapon['spells']), 3)

        spell_1 = weapon['spells'][0]
        self.assertEqual(spell_1['spell'], 'cast_on_critical_strike')
        self.assertEqual(spell_1['trigger'], {'spell': 'stun'})

        spell_2 = weapon['spells'][1]
        self.assertEqual(spell_2['spell'], 'haste')

        spell_3 = weapon['spells'][2]
        self.assertEqual(spell_3['spell'], 'cast_on_hit')

        trigger = spell_3['trigger']
        self.assertEqual(isinstance(trigger, dict), True)
        self.assertEqual(trigger['spell'], 'vulnerability')
        self.assertEqual(trigger['trigger'], {'spell': 'cast_on_curse', 'trigger': {'spell': 'ignite burning_ground'}})


if __name__ == '__main__':
    unittest.main()
